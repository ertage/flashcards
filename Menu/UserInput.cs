namespace flashcards
{
    class UserInput
    {
        public string NameEntry()
        {
            var nameEntry = Console.ReadLine();
            return nameEntry;
        }

        public int IdEntry()
        {
            string Input = Console.ReadLine();
            int id;
            while(!int.TryParse(Input, out id) || id <= 0)
            {
                Console.Write("Not a valid input, try again:");
                Input = Console.ReadLine();
            }
            return id;
        }
        public void Continue()
        {
        Console.WriteLine("Press any key to continue");
                    Console.ReadKey();
        }


    }
}