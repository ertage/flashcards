using studyarea;

namespace flashcards
{
    class Menu
    {

        UserInput ui = new UserInput();

        StacksDB stacksDB = new StacksDB();
        FlashCardsDB flashcardsDB = new FlashCardsDB();
        StudyMenu studyMenu = new StudyMenu();
        studyarea.Study studysession = new studyarea.Study();
        private string stackChooser;
        
        public void MainMenu()
        {
            bool check = true;
            while(check)
            {
            Console.Clear();
            Console.WriteLine("############################");
            Console.WriteLine("MENU");
            Console.WriteLine("1 to Manage Stacks");
            Console.WriteLine("2 to Manage Flashcards");
            Console.WriteLine("3 to Study");
            Console.WriteLine("4 to Show Study Sessions");
            Console.WriteLine("0 to Quit Application");
            Console.WriteLine("############################");
            Console.Write("Choose an Option:");
            string menuOption = Console.ReadLine();

            switch(menuOption)
            {
                case "1": 
                StackMenu();
  			  	break;
				case "2":
                Console.Clear();
                stacksDB.ShowStacks();
                stackChooser = stacksDB.stackValidation(); //Checks if the picked stack exists
                if(stackChooser != null)
                {
                    CardMenu(stackChooser);
                }
  				break;
				case "3":
                Console.Clear();
                stacksDB.ShowStacks();
                stackChooser = stacksDB.stackValidation(); //Checks if the picked stack exists
                if(stackChooser != null)
                {
                    studyMenu.StudyAreaMenu(stackChooser);
                }
                break;
                case "4":
                studysession.StudySessionData();
				break;
				case "0":
                    Environment.Exit(0);
				break;
  				default:
				  Console.WriteLine("Not a Valid Input! Try again!");
  				break;
			}
            }
        }

        private void StackMenu()
        {
            Console.Clear();
            bool check = true;
            while(check)
            {
            Console.WriteLine("############################");
            Console.WriteLine("STACK MENU");
            Console.WriteLine("1 to show current stack");
            Console.WriteLine("2 to add new stack");
            Console.WriteLine("3 to modify stack");
            Console.WriteLine("4 to delete stack");
            Console.WriteLine("0 to leave stack menu");
            Console.WriteLine("############################");
            Console.Write("Choose an option:");
            string menuOption = Console.ReadLine();

            switch(menuOption)
            {
                case "1": 
                Console.Clear();
                stacksDB.ShowStacks();
                ui.Continue();
  			  	break;
				case "2":
                Console.Clear();
                stacksDB.FillStacks();
  				break;
				case "3":
                Console.Clear();
                stacksDB.ShowStacks();
                stacksDB.EditStacks();
				break;
				case "4":
                Console.Clear();
                stacksDB.ShowStacks();
                stacksDB.DeleteStacks();
				break;
				case "0":
                check = false;
				break;
  				default:
                Console.Clear();
				Console.WriteLine("Not a Valid Input! Try again!");
  				break;
			}
            }
        }

        private void CardMenu(string stack)
        {
            string currentStack = stack;
            bool check = true;
            while(check)
            {
                Console.WriteLine("Current working stack {0}", currentStack);
                Console.WriteLine("############################");
                Console.WriteLine("FLASHCARD MENU");
                Console.WriteLine("1 to change a stack");
                Console.WriteLine("2 to view all flashcards in stack");
                Console.WriteLine("3 to add a new flashcard");
                Console.WriteLine("4 to edit a flashcard");
                Console.WriteLine("5 to delete a flashcard");
                Console.WriteLine("0 to leave card menu");
                Console.WriteLine("############################");
                Console.Write("Choose an option:");
                string menuOption = Console.ReadLine();

                switch(menuOption)
                {
                    case "1": 
                    Console.Clear();
                    stacksDB.ShowStacks();
                    string newStack = stacksDB.stackValidation();
                    if (newStack != null)
                    {
                        CardMenu(newStack);
                    }
  		    	  	break;
				    case "2":
                    Console.Clear();
                    flashcardsDB.ShowCards(currentStack);
                    ui.Continue();
  		    		break;
				    case "3":
                    Console.Clear();
                    int stackID = stacksDB.GetEntryInt(currentStack);
                    flashcardsDB.FillFlashcards(currentStack, stackID);
				    break;
                    case "4":
                    Console.Clear();
                    flashcardsDB.ShowCards(currentStack);
                    flashcardsDB.EditCards(currentStack);
                    break;
				    case "5":
                    Console.Clear();
                    flashcardsDB.ShowCards(currentStack);
                    flashcardsDB.DeleteCards(currentStack);
				    break;
				    case "0":
                    check = false;
				    break;
  		    		default:
				      Console.WriteLine("Not a Valid Input! Try again!");
  		    		break;
			    }
            }
        }



    }
}