using System.Data.SqlClient;
using System.Configuration;

namespace flashcards
{
    class CreateTables
    {
        public static string cs = ConfigurationManager.ConnectionStrings["cs"].ConnectionString;
        public void CreateTableStacks()
        {

            using (SqlConnection connection = new SqlConnection(cs))  
            {  
                using (SqlCommand command = connection.CreateCommand())
                {
                    connection.Open();
                //command.CommandText = "DROP TABLE IF EXISTS flashcards";
                //command.ExecuteNonQuery();
                //command.CommandText = "DROP TABLE IF EXISTS stacks";
                //command.ExecuteNonQuery();
                //command.CommandText = "DROP TABLE IF EXISTS study";
                //command.ExecuteNonQuery();
                    command.CommandText =
                        @"IF OBJECT_ID('dbo.stacks', 'U') IS NULL 
                        CREATE TABLE stacks(
                            StacksID INTEGER IDENTITY(1,1) NOT NULL PRIMARY KEY, 
                            Subject VARCHAR(255) UNIQUE
                        )";
                    command.ExecuteNonQuery();
                }
            }
        }        

        public void CreateTableFlashcards()
        {
            using (SqlConnection connection = new SqlConnection(cs))  
            {  
                using (SqlCommand command = connection.CreateCommand())
                {
                    connection.Open();
                    command.CommandText =
                        @"IF OBJECT_ID('dbo.flashcards', 'U') IS NULL
                            CREATE TABLE flashcards (
                            FlashcardsID INTEGER IDENTITY(1,1) NOT NULL PRIMARY KEY,
                            StacksID INTEGER FOREIGN KEY REFERENCES stacks(StacksID)
                            ON DELETE CASCADE
                            ON UPDATE CASCADE,
                            Front VARCHAR(255),
                            Back VARCHAR (255)
                            )";
                    command.ExecuteNonQuery();
                }
            }
        }        
        
        public void CreateTableStudy()
        {
            using (SqlConnection connection = new SqlConnection(cs))  
            {  
                using (SqlCommand command = connection.CreateCommand())
                {
                    connection.Open();
                    command.CommandText =
                            @"IF OBJECT_ID('dbo.study', 'U') IS NULL
                            CREATE TABLE study (
                            StudyID INTEGER IDENTITY(1,1) NOT NULL PRIMARY KEY,
                            StacksID INTEGER FOREIGN KEY REFERENCES stacks(StacksID)
                            ON DELETE CASCADE
                            ON UPDATE CASCADE,
                            CDate DATE,
                            Score INTEGER
                            )";
                    command.ExecuteNonQuery();

                }
            }
        }












    }
}