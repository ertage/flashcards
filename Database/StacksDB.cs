using System.Data.SqlClient;
using System.Configuration;
using ConsoleTableExt;

namespace flashcards
{
    class StacksDB
    {
        private string cs = ConfigurationManager.ConnectionStrings["cs"].ConnectionString;
        private List<StacksList> stacksList = new List<StacksList>();
        UserInput ui = new UserInput();

        public void FillStacks()
        {
            using (SqlConnection connection = new SqlConnection(this.cs))  
            {  
                using (SqlCommand command = connection.CreateCommand())
                {
                Console.Write("Enter Subject:");
                string subjectEntry = ui.NameEntry();
                connection.Open();
                command.CommandText =
                        @"INSERT INTO stacks (Subject)
                        VALUES (@subjectInput 
                        )";
                command.Parameters.Add(new SqlParameter("@subjectInput", subjectEntry));

                command.ExecuteNonQuery();
                }
            }
        }

        public void ShowStacks()
        {

            using (SqlConnection connection = new SqlConnection(this.cs))  
            {  
                using (SqlCommand command = connection.CreateCommand())
                    {
                    connection.Open();
                    command.CommandText = @"SELECT count(*) FROM stacks";
                    object result = command.ExecuteScalar();
                    int resultCount = Convert.ToInt32(result);
                    if (resultCount > 0)
                    {
                        Console.WriteLine("Current available stack subjects:");
                        string output = 
                                @"SELECT * FROM stacks 
                                ORDER BY StacksId ASC
                                ";
                        SqlCommand command1 = new SqlCommand(output, connection);
                        SqlDataReader reader = command1.ExecuteReader();
                        while (reader.Read() == true)
                        {
                            this.stacksList.Add(new StacksList() {Subject = reader["Subject"].ToString()});
                        }
                    }
                    else
                    {
                        Console.WriteLine("There are no entries yet");
                    }
                }
                ConsoleTableBuilder
                .From(this.stacksList)
                .ExportAndWrite();
                this.stacksList.Clear();
                Console.WriteLine();
            }

        }

        public void EditStacks()
        {
            using (SqlConnection connection = new SqlConnection(this.cs))  
            {  
                using (SqlCommand command = connection.CreateCommand())
                {
                    Console.Write("Enter name of entry you want to change:");
                    string oldSubject = ui.NameEntry();
                    string newSubject ="";
                    connection.Open();
                    command.Parameters.Add(new SqlParameter("@oldSubject", oldSubject));
                    command.CommandText = 
                            @"SELECT count(*) 
                            FROM stacks 
                            WHERE Subject = (@oldSubject)
                            ";

                    object result = command.ExecuteScalar();
                    int resultCount = Convert.ToInt32(result);
                    if (resultCount == 1)
                    {
                        Console.Write("Enter new subject:");
                        newSubject = ui.NameEntry();
                        command.CommandText = 
                                @"UPDATE stacks 
                                SET Subject = @newSubject 
                                WHERE Subject = @oldSubject
                                ";
                        command.Parameters.Add(new SqlParameter("@newSubject", newSubject));
                    }
                    else
                    {
                        Console.WriteLine("This entry doesn't exist, try another one");
                    }
                    command.ExecuteNonQuery();

                }
            }
        }
        public void DeleteStacks()
        {
            using (SqlConnection connection = new SqlConnection(this.cs))  
            {  
                using (SqlCommand command = connection.CreateCommand())
                {
                    Console.Write("Enter name of the subject you want to delete:");
                    string subject = ui.NameEntry();
                    connection.Open();
                    command.CommandText = 
                            @"SELECT count(*) 
                            FROM stacks 
                            WHERE Subject = (@subject)
                            ";
                    command.Parameters.Add(new SqlParameter("@subject", subject));
                    object result = command.ExecuteScalar();
                    int resultCount = Convert.ToInt32(result);
                    if (resultCount == 1)
                    {
                    command.CommandText = 
                            @"DELETE FROM stacks 
                            WHERE Subject = (@subject)
                            ";
                    Console.WriteLine("You deleted entry {0}", subject);
                    }
                    else
                    {
                        Console.WriteLine("This entry doesn't exist, try another one");
                    }
                    command.ExecuteNonQuery();
                }
            }
        }

        public string stackValidation()
        {
            using (SqlConnection connection = new SqlConnection(cs))  
            {  
                using (SqlCommand command = connection.CreateCommand())
                {
                    Console.Write("Choose stack:");
                    string subject = Console.ReadLine();

                    connection.Open();
                    command.CommandText = 
                            @"SELECT count(*) 
                            FROM stacks 
                            WHERE Subject = (@subject)
                            ";
                    command.Parameters.Add(new SqlParameter("@subject", subject));
                    object result = command.ExecuteScalar();
                    int resultCount = Convert.ToInt32(result);
                    if (resultCount == 1)
                    {
                        return subject;
                    }
                    else
                    {
                        Console.WriteLine("This entry doesn't exist, try another one");
                    }
                    command.ExecuteNonQuery();

                    return null;


                }
            }

        }

        public int GetEntryInt(string stackSubject)
        {
            using (SqlConnection connection = new SqlConnection(cs))  
            {  
                using (SqlCommand command = connection.CreateCommand())
                {
                   connection.Open(); 
                   command.CommandText = 
                            @"SELECT StacksID
                            FROM stacks 
                            WHERE Subject = (@stackSubject)
                            ";
                    command.Parameters.Add(new SqlParameter("@stackSubject", stackSubject));
                    int stackID = (int)command.ExecuteScalar();

                    return stackID;
                }
            }

        }




    }
}