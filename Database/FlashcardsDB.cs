using System.Data.SqlClient;
using System.Configuration;
using ConsoleTableExt;

namespace flashcards
{
    class FlashCardsDB
    {
        private static string cs = ConfigurationManager.ConnectionStrings["cs"].ConnectionString;
        UserInput ui = new UserInput();

        public void FillFlashcards(string stackSubject, int stackID)
        {
            using (SqlConnection connection = new SqlConnection(cs))  
            {  
                using (SqlCommand command = connection.CreateCommand())
                {
                    connection.Open();
                    Console.Write("Fill front of card:");
                    string front = ui.NameEntry();
                    Console.Write("Fill back of card:");
                    string back = ui.NameEntry();
                    command.CommandText =
                            @"INSERT INTO flashcards (StacksID, Front, Back)
                            VALUES (@stacksID, @front, @back 
                            )";
                    command.Parameters.Add(new SqlParameter("@front", front));
                    command.Parameters.Add(new SqlParameter("@back", back));
                    command.Parameters.Add(new SqlParameter("@stacksID", stackID));
                    command.ExecuteNonQuery();
                }
            }

        } 


        public void ShowCards(string stack)
        {

           var cardsList = GetCards(stack); 

                ConsoleTableBuilder
                .From(cardsList)
                .WithTitle(stack)
                .ExportAndWrite();
                cardsList.Clear();
                Console.WriteLine();
        }

        public List<FlashCardsToViewDTO> GetCards(string stack)
        {
            List<FlashCardsToViewDTO> cardsList = new List<FlashCardsToViewDTO>();
            int i = 1;
            string stackName = stack;
            
            using (SqlConnection connection = new SqlConnection(cs))  
            {  
                using (SqlCommand command = connection.CreateCommand())
                    {
                    connection.Open();
                    command.CommandText = 
                            @"SELECT count(*) 
                            FROM flashcards a LEFT JOIN stacks b
                            ON (b.StacksID = a.StacksID)
                            WHERE b.Subject = (@stackName)
                            ";
                    command.Parameters.Add(new SqlParameter("@stackName", stackName));
                    object result = command.ExecuteScalar();
                    int resultCount = Convert.ToInt32(result);
                    if (resultCount > 0)
                    {
                        Console.WriteLine("Current available flashcards");
                        string output = 
                                @"SELECT FlashcardsID, Front, Back 
                                FROM flashcards a LEFT OUTER JOIN stacks b 
                                ON (b.StacksID = a.StacksID) 
                                WHERE b.Subject = (@stackName)
                                ";
                        SqlCommand command1 = new SqlCommand(output, connection);
                        command1.Parameters.Add(new SqlParameter("@stackName", stackName));
                        SqlDataReader reader = command1.ExecuteReader();
                        while (reader.Read() == true)
                        {
                            cardsList.Add(new FlashCardsToViewDTO() {Id = i++, Front = reader["Front"].ToString(), Back = reader["Back"].ToString()});
                        }
                    }
                    else
                    {
                        Console.WriteLine("There are no entries yet");
                    }
                  
                    }
            }

            return cardsList;

        }

        public void DeleteCards(string currentStack)
        {
            using (SqlConnection connection = new SqlConnection(cs))  
            {  
                using (SqlCommand command = connection.CreateCommand())
                {
                    Console.Write("Enter id of the flashcard you want to delete:");
                    int id = ui.IdEntry();
                    connection.Open();
                    command.CommandText = 
                            @"SELECT FlashcardsID 
                            FROM flashcards a
                            LEFT JOIN stacks b 
                            ON (b.StacksID = a.StacksID)
                            WHERE b.Subject = @currentStack
                            ORDER BY FlashcardsID
                            OFFSET (@id) ROWS FETCH NEXT 1 ROWS ONLY
                            ";
                    command.Parameters.Add(new SqlParameter("@id", id-1));
                    command.Parameters.Add(new SqlParameter("@currentStack", currentStack));
                    object result = command.ExecuteScalar();
                    int resultCount = Convert.ToInt32(result);
                    if (resultCount > 0)
                    {
                        command.Parameters.Add(new SqlParameter("@result", result));
                        command.CommandText = 
                                @"DELETE FROM flashcards 
                                WHERE FlashcardsID = (@result)
                                ";
                        Console.WriteLine("You deleted entry #{0}", id);
                    }
                    else
                    {
                        Console.WriteLine("This entry doesn't exist, try another one");
                    }
                    command.ExecuteNonQuery();
                }
            }
        }

        public void EditCards(string currentStack)
        {
            using (SqlConnection connection = new SqlConnection(cs))  
            {  
                using (SqlCommand command = connection.CreateCommand())
                {
                    Console.Write("Enter id of the flashcard you want to edit:");
                    int id = ui.IdEntry();
                    connection.Open();
                    command.CommandText = 
                            @"SELECT FlashcardsID 
                            FROM flashcards a
                            LEFT JOIN stacks b 
                            ON (b.StacksID = a.StacksID)
                            WHERE b.Subject = @currentStack
                            ORDER BY FlashcardsID
                            OFFSET (@id) ROWS FETCH NEXT 1 ROWS ONLY
                            ";
                    command.Parameters.Add(new SqlParameter("@id", id-1));
                    command.Parameters.Add(new SqlParameter("@currentStack", currentStack));
                    object result = command.ExecuteScalar();
                    int resultCount = Convert.ToInt32(result);
                    if (resultCount > 0)
                    {
                        Console.Write("Enter new front card:");
                        string newFront = ui.NameEntry();
                        Console.Write("Enter new back card:");
                        string newBack = ui.NameEntry();
                        command.CommandText = 
                                @"UPDATE flashcards 
                                SET Front = @newFront, Back = @newBack 
                                WHERE FlashcardsID = (@flashcardID)
                                ";
                        command.Parameters.Add(new SqlParameter("@newFront", newFront));
                        command.Parameters.Add(new SqlParameter("@newBack", newBack));
                        command.Parameters.Add(new SqlParameter("@flashcardID", resultCount));
                    }
                    else
                    {
                        Console.WriteLine("This entry doesn't exist, try another one");
                    }
                    command.ExecuteNonQuery();


                }
            }
        }


    }
}