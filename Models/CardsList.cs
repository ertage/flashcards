using System;

namespace flashcards
{
    public class CardsList
    {
        public int ID {get; set;}
        public string Front {get; set;}
        public string Back {get; set;}
    }

}
