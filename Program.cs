﻿using System.Data.SqlClient;
using System.Configuration;

namespace flashcards
{
    class Program
    {
        static void Main(string[] args)
        {

            CreateTables ct = new CreateTables();
            Menu menu = new Menu();
            ct.CreateTableStacks();
            ct.CreateTableFlashcards();
            ct.CreateTableStudy();
            menu.MainMenu();
            
        }
    }
}