# FlashCards

A FlashCards CLI program written in C# with a MSSQL Database.

A simple way to study and keep track of your progress.

## Features

- You can create a stack with the topic you want to study
- Flashcards with front and back side 
- Edit, Delete, Add new stacks/flashcards
- Work through your flashcards in the study area which automatically keeps score for you


## Sample Screenshots

![Main Menu](demo/mainmenu.png)

![Flash Card Menu](/demo/flashcardmenu.png)

![Session History](/demo/sessionhistory.png)


## Things I have learned and improved in

 - Connecting C# to a SQL Database
 - Using Azure Data Studio
 - C# Debugging
 - CRUD
 - ConsoleTableExt library for a nicer table view
 - Seperation of Concerns

 ## Used Packages

 - [ConsoleTableExt](https://github.com/minhhungit/ConsoleTableExt)
 - [Connection String](https://docs.microsoft.com/en-us/dotnet/api/system.configuration.configurationmanager.connectionstrings?view=dotnet-plat-ext-6.0)
 - [SQLClient](https://docs.microsoft.com/en-us/dotnet/api/system.data.sqlclient?view=windowsdesktop-6.0)

## Instructions

To get started you have to add your connectionString inside the app.config

[Connection String](https://docs.microsoft.com/en-us/dotnet/api/system.configuration.configurationmanager.connectionstrings?view=dotnet-plat-ext-6.0)



