public class StudyFrontDTO
{
        public string Front {get; set;}
}

public class StudyBackDTO
{
        public string Back {get; set;}
}

public class StudySessionDTO 
{
        public string Subject { get; set; }
        public string January { get; set; }
        public string February { get; set; }
        public string March { get; set; }
        public string April { get; set; }
        public string May { get; set; }
        public string June { get; set; }
        public string July { get; set; }
        public string August { get; set; }
        public string September { get; set; }
        public string October { get; set; }
        public string November { get; set; }
        public string December { get; set; }
        //public int Sessions { get; set; }
}