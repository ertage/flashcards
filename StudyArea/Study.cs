using System.Data.SqlClient;
using System.Configuration;
using ConsoleTableExt;
using flashcards;

namespace studyarea
{
    class Study
    {
        private static string cs = ConfigurationManager.ConnectionStrings["cs"].ConnectionString;
        private List<StudyList> studyList = new List<StudyList>();
        //private int? currentStudyID = null;
        private int addScore = 0;
        UserInput ui = new UserInput();

        //public void StartStudy(string currentCardFront, string currentCardBack, string currentStack, string answer)
        //{
        //    if(answer == currentCardBack)
        //    {
        //        addScore++;
        //        if(currentStudyID == null)
        //        {
        //        Console.WriteLine("Your answer is correct");
        //        currentStudyID = CorrectAnswer(addScore, currentStack);
        //        ui.Continue();
        //        }
        //        else
        //        {
        //        Console.WriteLine("Your answer is correct");
        //        CorrectAnswer(addScore, currentStudyID);
        //        ui.Continue();
        //        }
        //    }
        //    else 
        //    {
        //        Console.WriteLine("Your answer is wrong");
        //        ui.Continue();
        //    }
        //}

//Loads first card, starts the study when finished loads next card
        public void ShowCurrentCard(string currentStack)
        {
            int cardQuantity = CardQuantity(currentStack);
            int cardNumber = 0;
            while(cardNumber < cardQuantity)
            {
            Console.Clear();
            string currentCardFront = GetCurrentCardFront(currentStack, cardNumber);
            string currentCardBack = GetCurrentCardBack(currentStack, cardNumber);
            studyList.Add(new StudyList() {Front = currentCardFront});
            ConsoleTableBuilder
                .From(studyList)
                .WithTitle(currentStack)
                .ExportAndWrite();
                studyList.Clear();
                Console.WriteLine();
            Console.Write("Enter your answer or Q to quit:");
            string answer = Console.ReadLine();
            if(answer == "q" || answer == "Q")
            {
                Console.WriteLine("You have {0} out of {1} correct", addScore, cardQuantity);
                CorrectAnswer(addScore, currentStack);
                ui.Continue();
                return;
            }
            else
            {
                if(answer == currentCardBack)
                {
                    Console.WriteLine("Your answer is correct");
                    addScore++;
                    
                    ui.Continue();
                }
                else 
                {
                    Console.WriteLine("Your answer is wrong");
                    ui.Continue();
                }

            cardNumber++;
            }
            }
            Console.WriteLine("You have {0} out of {1} correct", addScore, cardQuantity);
            CorrectAnswer(addScore, currentStack);
            addScore = 0;
            ui.Continue();
            //currentStudyID = null;
        }

//Gets the Value of the Front Card
        public string GetCurrentCardFront(string currentStack, int cardNumber)
        {
            StudyFrontDTO studyFront = new StudyFrontDTO();
            string stackName = currentStack;
            
            using (SqlConnection connection = new SqlConnection(cs))  
            {  
                using (SqlCommand command = connection.CreateCommand())
                    {
                    connection.Open();
                    command.CommandText = 
                            @"SELECT count(*) 
                            FROM flashcards a LEFT JOIN stacks b
                            ON (b.StacksID = a.StacksID)
                            WHERE b.Subject = (@stackName)
                            ";
                    command.Parameters.Add(new SqlParameter("@stackName", stackName));
                    command.Parameters.Add(new SqlParameter("@cardNumber", cardNumber));
                    object result = command.ExecuteScalar();
                    int resultCount = Convert.ToInt32(result);
                    if (resultCount > 0)
                    {
                        command.CommandText = 
                                @"SELECT Front
                                FROM flashcards a LEFT OUTER JOIN stacks b 
                                ON (b.StacksID = a.StacksID) 
                                WHERE b.Subject = (@stackName)
                                ORDER BY FlashcardsID
                                OFFSET (@cardNumber) ROWS FETCH NEXT 1 ROWS ONLY
                                ";
                        result = command.ExecuteScalar();
                        string resultName = result.ToString();
                        studyFront.Front = resultName;
                    }
                    else
                    {
                    Console.WriteLine("There are no entries yet");
                    }
                  
                    }
            }

            return studyFront.Front;
        }

//Gets the value of the Back card
        public string GetCurrentCardBack(string currentStack, int cardNumber)
        {
            StudyBackDTO studyBack = new StudyBackDTO();
            string stackName = currentStack;
            
            using (SqlConnection connection = new SqlConnection(cs))  
            {  
                using (SqlCommand command = connection.CreateCommand())
                    {
                    connection.Open();
                    command.CommandText = 
                            @"SELECT count(*) 
                            FROM flashcards a LEFT JOIN stacks b
                            ON (b.StacksID = a.StacksID)
                            WHERE b.Subject = (@stackName)
                            ";
                    command.Parameters.Add(new SqlParameter("@stackName", stackName));
                    command.Parameters.Add(new SqlParameter("@cardNumber", cardNumber));
                    object result = command.ExecuteScalar();
                    int resultCount = Convert.ToInt32(result);
                    if (resultCount > 0)
                    {
                        command.CommandText = 
                                @"SELECT Back
                                FROM flashcards a LEFT OUTER JOIN stacks b 
                                ON (b.StacksID = a.StacksID) 
                                WHERE b.Subject = (@stackName)
                                ORDER BY FlashcardsID
                                OFFSET (@cardNumber) ROWS FETCH NEXT 1 ROWS ONLY
                                ";
                        result = command.ExecuteScalar();
                        string resultName = result.ToString();
                        studyBack.Back = resultName;
                    }
                    else
                    {
                    Console.WriteLine("There are no entries yet");
                    }
                  
                    }
            }

            return studyBack.Back;
        }

//Gets the Card Quantity of the stack
        public int CardQuantity(string currentStack)
        {
            int cardQuantity;

            using (SqlConnection connection = new SqlConnection(cs))  
            {  
                using (SqlCommand command = connection.CreateCommand())
                    {   
                        connection.Open();

                        command.CommandText = 
                            @"SELECT count(*) 
                            FROM flashcards a LEFT JOIN stacks b
                            ON (b.StacksID = a.StacksID)
                            WHERE b.Subject = (@stackName)
                            ";
                        command.Parameters.Add(new SqlParameter("@stackName", currentStack));
                        object result = command.ExecuteScalar();
                        cardQuantity = Convert.ToInt32(result);
                        Console.WriteLine(cardQuantity);
                    }
            }
            return cardQuantity;
        }

//Initial entry for the study session and adds score
        public void CorrectAnswer(int addScore, string currentStack)
        {
            using (SqlConnection connection = new SqlConnection(cs))  
            {  
                using (SqlCommand command = connection.CreateCommand())
                    {   

                        DateTime dateNow = DateTime.Now;
                        int month = dateNow.Month;
                        string dater = dateNow.ToShortDateString();

                        connection.Open();

                        command.CommandText =
                                @"SELECT StacksID
                                FROM stacks
                                WHERE Subject = @currentStack
                                ";
                        command.Parameters.Add(new SqlParameter("@currentStack", currentStack));
                        object result = command.ExecuteScalar();
                        int currentStackID = Convert.ToInt32(result);
                        SqlCommand command1 = connection.CreateCommand();
                        command1.CommandText =
                            @"INSERT INTO study (StacksID, Score, CDate)
                            VALUES (@currentStackID, @addScore, @dateNow)
                            SELECT SCOPE_IDENTITY()
                            ";
                        command1.Parameters.Add(new SqlParameter("@addScore", addScore));
                        command1.Parameters.Add(new SqlParameter("@dateNow", dater));
                        command1.Parameters.Add(new SqlParameter("@currentStackID", currentStackID));
                        command1.ExecuteNonQuery();
                        //object result1 = command1.ExecuteScalar();
                        //int currentStudyID = Convert.ToInt32(result1);
                        //return currentStudyID;
                    }
            }
        }
//Adds score to current study session
        //public void CorrectAnswer(int addScore, int? currentID)
        //{
        //    using (SqlConnection connection = new SqlConnection(cs))  
        //    {  
        //        using (SqlCommand command = connection.CreateCommand())
        //            {   
        //                connection.Open();
        //                command.CommandText =
        //                    @"UPDATE study 
        //                    SET Score = @addScore
        //                    WHERE StudyID = @currentID
        //                    ";
        //                command.Parameters.Add(new SqlParameter("@addScore", addScore));
        //                command.Parameters.Add(new SqlParameter("@currentID", currentID));
        //                command.ExecuteNonQuery();
        //            }
        //    }
        //}

        public void StudySessionData()
        {
            using (SqlConnection connection = new SqlConnection(cs))  
            {  
                using (SqlCommand command = connection.CreateCommand())
                {
                    List<StudySessionDTO> studySessions = new List<StudySessionDTO>();
                    Console.Write("Which year?");
                    int yearInput = Convert.ToInt32(Console.ReadLine());
                    connection.Open();
                    command.CommandText =
                            @"SELECT DATENAME(year,CDATE) from study WHERE DATENAME(year,CDATE) = @yearInput
                            ";
                    command.Parameters.Add(new SqlParameter("@yearInput", yearInput));
                    object resultYear = command.ExecuteScalar();
                    string year = resultYear.ToString();

                    SqlCommand command1 = connection.CreateCommand();

                    command1.CommandText =
                            @"SELECT * FROM (SELECT StudyID, DATENAME(month, CDate) month, Subject FROM study a LEFT JOIN stacks b ON (b.StacksID = a.StacksID) 
                            WHERE DATENAME(year,CDATE) = @yearinput) t
                            PIVOT(
                            COUNT(StudyID) 
                            FOR month IN (
                            [January],
                            [February],
                            [March],
                            [April],
                            [May],
                            [June],
                            [July],
                            [August],
                            [September],
                            [October],
                            [November],
                            [December]
                            )
                        ) AS pivot_table";
                        command1.Parameters.Add(new SqlParameter("@yearInput", yearInput));
                        SqlDataReader reader = command1.ExecuteReader();
                        while (reader.Read() == true)
                        {
                            studySessions.Add(new StudySessionDTO() {Subject= reader[0].ToString(), 
                            January = reader[1].ToString(),
                            February = reader[2].ToString(),
                            March = reader[3].ToString(),
                            April = reader[4].ToString(),
                            May = reader[5].ToString(),
                            June = reader[6].ToString(),
                            July = reader[7].ToString(),
                            August = reader[8].ToString(),
                            September = reader[9].ToString(),
                            October = reader[10].ToString(),
                            November = reader[11].ToString(),
                            December = reader[12].ToString()
                            }); 
                        }
                    ConsoleTableBuilder
                    .From(studySessions)
                    .WithTitle(year)
                    .ExportAndWrite();
                    Console.WriteLine();
                    ui.Continue();
                }
            } 
        }


    }

}