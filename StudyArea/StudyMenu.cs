using flashcards;

namespace studyarea
{
    class StudyMenu
    {
        flashcards.StacksDB stacksDB = new StacksDB();
        flashcards.UserInput ui = new UserInput();
        Study study = new Study();

       public void StudyAreaMenu(string currentStack)
        {
            bool check = true;
            while(check)
            {
            Console.Clear();
            Console.WriteLine("############################");
            Console.WriteLine("STUDY AREA");
            Console.WriteLine("Current study subject is: {0}", currentStack);
            Console.WriteLine("1 to change subject");
            Console.WriteLine("2 to begin studying");
            Console.WriteLine("0 to leave study area");
            Console.WriteLine("############################");
            Console.Write("Choose an Option:");
            string menuOption = Console.ReadLine();

            switch(menuOption)
            {
                case "1": 
                stacksDB.ShowStacks();
                string newStack = stacksDB.stackValidation();
                if (newStack != null)
                {
                    StudyAreaMenu(newStack);
                }
                ui.Continue();
  			  	break;
				case "2":
                Console.Clear();
                study.ShowCurrentCard(currentStack);
  				break;
				case "0":
                    check = false;
				break;
  				default:
				  Console.WriteLine("Not a Valid Input! Try again!");
  				break;
			}
            }
        } 





    }
}